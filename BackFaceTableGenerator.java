public class BackFaceTableGenerator extends TableGenerator
{
	int[][] transitionTableCross;
	int[][] transitionTableCorner;
	
	int[] inverseTableCross;
	int[] inverseTableCorner;
	
	public BackFaceTableGenerator(int[][] transitionTableCross, int[][] transitionTableCorner, int[] inverseTableCross, int[] inverseTableCorner)
	{
		this.transitionTableCross = transitionTableCross;
		this.transitionTableCorner = transitionTableCorner;
		this.inverseTableCross = inverseTableCross;
		this.inverseTableCorner = inverseTableCorner;
		entrySize = 3009871872L; // = 7 * 12^8
		fileName = "backFace.table";
		tableName = "BackFaceTable";
		maxDistance = 8;
		iterativeDeepingDepth = 6;
		clockCnt = 9;
	}
	
	protected int depthFirst(int stateCross, int stateCorner, int depth, int depthLeft, int lastMove)
	{
		long idx = computeIdx(stateCross, stateCorner);
		int entry = table.getEntry(idx);
		
		if (depthLeft == 0)
		{
			if (entry == maxDistance)
			{ //IDA => maxDistance means not yet visited
				table.setEntry(idx, depth);
				return 1;
			}
			else
			{
				return 0;
			}
		}
		
		if (entry != depth - depthLeft)
			return 0;
		
		int cnt = 0;
		for (int move = lastMove + 1; move < 15; move++)
		{
			for (int moveCnt = 1; moveCnt < 12; moveCnt++)
			{
				stateCross  = transitionTableCross[stateCross][move];
				stateCorner = transitionTableCorner[stateCorner][move];
				cnt += depthFirst(stateCross, stateCorner, depth, depthLeft - 1, move);
			}
			stateCross  = transitionTableCross[stateCross][move];
			stateCorner = transitionTableCorner[stateCorner][move];
		}
		
		return cnt;
	}
	
	private long computeIdx(int stateCross, int stateCorner)
	{
		if (stateCorner >= 12096) //7 * 12^3
		{
			stateCross  = inverseTableCross[stateCross];
			stateCorner = inverseTableCorner[stateCorner];
		}
		
		return (stateCorner * 248832L) + stateCross;
	}
	
	protected int solveOptimal(long idx)
	{
		int stateCross  = (int)(idx % 248832);
		int stateCorner = (int)(idx / 248832);
		
		for (int depth = iterativeDeepingDepth + 1; depth < maxDistance; depth++)
		{
			if (solveOptimalDepthFirst(stateCross, stateCorner, depth - iterativeDeepingDepth, iterativeDeepingDepth - 1))
				return depth;
		}
		
		return maxDistance;
	}
	
	private boolean solveOptimalDepthFirst(int stateCross, int stateCorner, int depthLeft, int lastMove)
	{
		long idx = computeIdx(stateCross, stateCorner);
		int entry = table.getEntry(idx);
		
		if (entry == depthLeft + iterativeDeepingDepth)
		{
			return true;
		}
		else if (depthLeft == 0)
		{
			return false;
		}
		
		for (int move = lastMove + 1; move < 15; move++)
		{
			for (int moveCnt = 1; moveCnt < 12; moveCnt++)
			{
				stateCross = transitionTableCross[stateCross][move];
				stateCorner = transitionTableCorner[stateCorner][move];
				if (solveOptimalDepthFirst(stateCross, stateCorner, depthLeft - 1, move))
					return true;
			}
			stateCross = transitionTableCross[stateCross][move];
			stateCorner = transitionTableCorner[stateCorner][move];
		}
		
		return false;
	}
}
