import java.util.Scanner;

public class ClockSolver
{
	private int[][] transitionTableCross;
	private int[][] transitionTableCornerBack;
	private int[][] transitionTableCornerFront;
	
	private int[] inverseTableCross;
	private int[] inverseTableCorner;
	
	private int[] rotationTableCorner;
	
	private NibbleArray crossTable;
	private NibbleArray backFaceTable;
	
	private int lowerBoundBackCross;
	private int upperBoundTotal;
	private int[] bestSolutionFront;
	private int[] bestSolutionBack;
	
	public ClockSolver()
	{
		//generate transition tables
		byte[] moveManipulationCross = new byte[]{
			0b11100, //Uddd u1
			0b10110, //dUdd u1
			0b00111, //dddU u1
			0b01101, //ddUd u1
			0b11110, //UUdd u1
			0b10111, //dUdU u1
			0b01111, //ddUU u1
			0b11101, //UdUd u1
			0b11111, //UddU u1
			0b11111, //dUUd u1
			0b11111, //dUUU u1
			0b11111, //UdUU u1
			0b11111, //UUUd u1
			0b11111, //UUdU u1
			0b11111};//UUUU u1
		transitionTableCross = TransitionTableGenerator.generateTable(5, moveManipulationCross, false);
		
		byte[] moveManipulationCornerBack = new byte[]{
			0b1000, //Uddd u1
			0b0100, //dUdd u1
			0b0001, //dddU u1
			0b0010, //ddUd u1
			0b1100, //UUdd u1
			0b0101, //dUdU u1
			0b0011, //ddUU u1
			0b1010, //UdUd u1
			0b1001, //UddU u1
			0b0110, //dUUd u1
			0b0111, //dUUU u1
			0b1011, //UdUU u1
			0b1110, //UUUd u1
			0b1101, //UUdU u1
			0b1111};//UUUU u1
		transitionTableCornerBack = TransitionTableGenerator.generateTable(4, moveManipulationCornerBack, false);

		byte[] moveManipulationCornerFront = new byte[]{
			0b0100, //Uddd u1
			0b1000, //dUdd u1
			0b0010, //dddU u1
			0b0001, //ddUd u1
			0b1100, //UUdd u1
			0b1010, //dUdU u1
			0b0011, //ddUU u1
			0b0101, //UdUd u1
			0b0110, //UddU u1
			0b1001, //dUUd u1
			0b1011, //dUUU u1
			0b0111, //UdUU u1
			0b1101, //UUUd u1
			0b1110, //UUdU u1
			0b1111};//UUUU u1
		transitionTableCornerFront = TransitionTableGenerator.generateTable(4, moveManipulationCornerFront, true);
		
		//generate Tables for inverse Scramble
		inverseTableCross = new int[248832];
		for (int i = 0; i < 248832; i++)
		{
			int[] state = TransitionTableGenerator.idxToState(i, 5);
			for (int j = 0; j < 5; j++)
				state[j] = (12 - state[j]) % 12;
			inverseTableCross[i] = TransitionTableGenerator.stateToIdx(state);
		}
		
		inverseTableCorner = new int[20736];
		for (int i = 0; i < 20736; i++)
		{
			int[] state = TransitionTableGenerator.idxToState(i, 4);
			for (int j = 0; j < 4; j++)
				state[j] = (12 - state[j]) % 12;
			inverseTableCorner[i] = TransitionTableGenerator.stateToIdx(state);
		}
		
		//generate Table for rotation
		rotationTableCorner = new int[20736];
		for (int i = 0; i < 20736; i++)
		{
			int[] state = TransitionTableGenerator.idxToState(i, 4);
			int[] state2 = new int[]{(12 - state[1])%12, (12 - state[0])%12, (12 - state[3]) % 12, (12 - state[2])%12};
			rotationTableCorner[i] = TransitionTableGenerator.stateToIdx(state2);
		}		
		
		//generate pruning tables
		crossTable = new CrossTableGenerator(transitionTableCross).getTable();
		backFaceTable = new BackFaceTableGenerator(transitionTableCross, transitionTableCornerBack, inverseTableCross, inverseTableCorner).getTable();
	}
	
	public int solve(int stateCrossFront, int stateCrossBack, int stateCornerBack)
	{
		int lowerBoundFrontCross = crossTable.getEntry(stateCrossFront);
		lowerBoundBackCross = crossTable.getEntry(stateCrossBack);
		int[] solution = new int[15];
		bestSolutionFront = new int[15];
		bestSolutionBack = new int[15];
		boolean rotation = false;
		
		if (lowerBoundFrontCross > lowerBoundBackCross)
		{
			int tmp = stateCrossFront;
			stateCrossFront = stateCrossBack;
			stateCrossBack = tmp;
			stateCornerBack = rotationTableCorner[stateCornerBack];
			tmp = lowerBoundFrontCross;
			lowerBoundFrontCross = lowerBoundBackCross;
			lowerBoundBackCross = tmp;
			rotation = true;
		}
		
		upperBoundTotal = lowerBoundFrontCross + 9; //9, so that the for loop finds at least 1 solution
		
		for (int depthFront = lowerBoundFrontCross; depthFront < upperBoundTotal - lowerBoundBackCross; depthFront++)
		{
			iterativeDeepingDepthFirst(stateCrossFront, stateCrossBack, stateCornerBack, depthFront, depthFront, -1, solution);
		}
		
		String s;
		if (!rotation)
			s = solutionToString(bestSolutionFront, false) + solutionToString(bestSolutionBack, true);
		else
			s = solutionToString(bestSolutionBack, false) + solutionToString(bestSolutionFront, true);
		
		System.out.println("Optimal solution: " + s + " (" + upperBoundTotal + " moves)");
		
		return upperBoundTotal;
	}
	
	private String solutionToString(int[] moveCnts, boolean rotation)
	{
		String[] moveNames = {"Uddd", "dUdd", "dddU", "ddUd", "UUdd", "dUdU", "ddUU", "UdUd", 
				      "UddU", "dUUd", "dUUU", "UdUU", "UUUd", "UUdU", "UUUU"};
		String[] moveBackNames = {"UdUU", "dUUU", "UUdU", "UUUd", "ddUU", "dUdU", "UUdd", "UdUd", 
				      "UddU", "dUUd", "dUdd", "Uddd", "ddUd", "dddU", "UUUU"};
		
		String s = "";
		for (int move = 0; move < 15; move++)
		{
			if (moveCnts[move] != 0)
			{
				int moveCnt = moveCnts[move];
				
				if (!rotation)
					s += moveNames[move] + " u=" + moveCnt + " ";
				else
					s += moveBackNames[move] + " d=" + (12 - moveCnt)%12 + " ";
			}
		}
		
		return s;
	}
	
	private boolean iterativeDeepingDepthFirst(int stateCrossFront, int stateCrossBack, int stateCornerBack, int depthFront, int depthLeft, int lastMove, int[] solution)
	{
		int lowerBoundFrontCross = crossTable.getEntry(stateCrossFront);
		
		if (depthLeft == 0)
		{
			if (lowerBoundFrontCross == 0)
			{
				int backFace = backFaceTable.getEntry(computeFullSideIdx(stateCrossBack, stateCornerBack));
				if (depthFront + backFace < upperBoundTotal)
				{
					upperBoundTotal = depthFront + backFace;
					System.out.println("Solution with " + (depthFront + backFace) + " (" + depthFront + " + " + backFace + ")");
					
					for (int i = 0; i < 15; i++)
					{
						bestSolutionFront[i] = solution[i];
						bestSolutionBack[i] = 0;
					}
					
					for (int depthBackLeft = backFace; depthBackLeft > 0; depthBackLeft--)
					{
						int stateCrossBack2 = stateCrossBack;
						int stateCornerBack2 = stateCornerBack;
						
						boolean found = false;
						for (int move = 0; !found && move < 15; move++)
						{
							for (int moveCnt = 1; !found && moveCnt < 12; moveCnt++)
							{
								stateCrossBack2 = transitionTableCross[stateCrossBack2][move];
								stateCornerBack2 = transitionTableCornerBack[stateCornerBack2][move];
								if (backFaceTable.getEntry(computeFullSideIdx(stateCrossBack2, stateCornerBack2)) < depthBackLeft)
								{
									stateCrossBack = stateCrossBack2;
									stateCornerBack = stateCornerBack2;
									bestSolutionBack[move] = moveCnt;
									found = true;
								}
							}
							stateCrossBack2 = transitionTableCross[stateCrossBack2][move];
							stateCornerBack2 = transitionTableCornerBack[stateCornerBack2][move];
						}
					}
					
					if (depthFront == upperBoundTotal - lowerBoundBackCross)
						return true;
				}
			}
			return false;
		}
		
		if (lowerBoundFrontCross > depthLeft)
			return false;
		
		for (int move = lastMove + 1; move < 15; move++)
		{
			for (int moveCnt = 1; moveCnt < 12; moveCnt++)
			{
				stateCrossFront = transitionTableCross[stateCrossFront][move];
				stateCornerBack = transitionTableCornerFront[stateCornerBack][move];
				solution[move]++;
				if (iterativeDeepingDepthFirst(stateCrossFront, stateCrossBack, stateCornerBack, depthFront, depthLeft - 1, move, solution))
					return true;
			}
			stateCrossFront = transitionTableCross[stateCrossFront][move];
			stateCornerBack = transitionTableCornerFront[stateCornerBack][move];
			solution[move] = 0;
		}
		
		return false;
	}
	                    
	public int solveSuboptimal(int stateCrossFront, int stateCrossBack, int stateCornerBack, int upperBound)
	{
		int lowerBoundFrontCross = crossTable.getEntry(stateCrossFront);
		lowerBoundBackCross = crossTable.getEntry(stateCrossBack);
		
		if (lowerBoundFrontCross > lowerBoundBackCross)
		{
			int tmp = stateCrossFront;
			stateCrossFront = stateCrossBack;
			stateCrossBack = tmp;
			stateCornerBack = rotationTableCorner[stateCornerBack];
			tmp = lowerBoundFrontCross;
			lowerBoundFrontCross = lowerBoundBackCross;
			lowerBoundBackCross = tmp;			
		}
		
		upperBoundTotal = lowerBoundFrontCross + 9; //9, so that the for loop finds at least 1 solution
				
		for (int depthFront = lowerBoundFrontCross; depthFront < upperBoundTotal - lowerBoundBackCross; depthFront++)
		{
			if (iterativeDeepingDepthSuboptimal(stateCrossFront, stateCrossBack, stateCornerBack, depthFront, depthFront, -1, upperBound))
				return upperBoundTotal;
		}
		
		return upperBoundTotal;
	}
	
	private boolean iterativeDeepingDepthSuboptimal(int stateCrossFront, int stateCrossBack, int stateCornerBack, int depthFront, int depthLeft, int lastMove, int upperBound)
	{
		int lowerBoundFrontCross = crossTable.getEntry(stateCrossFront);
		
		if (depthLeft == 0)
		{
			if (lowerBoundFrontCross == 0)
			{
				int backFace = backFaceTable.getEntry(computeFullSideIdx(stateCrossBack, stateCornerBack));
				if (depthFront + backFace < upperBoundTotal)
				{
					upperBoundTotal = depthFront + backFace;
					if (upperBoundTotal <= upperBound || depthFront == upperBoundTotal - lowerBoundBackCross)
						return true;
				}
			}
			return false;
		}
		
		if (lowerBoundFrontCross > depthLeft)
			return false;
		
		for (int move = lastMove + 1; move < 15; move++)
		{
			for (int moveCnt = 1; moveCnt < 12; moveCnt++)
			{
				stateCrossFront = transitionTableCross[stateCrossFront][move];
				stateCornerBack = transitionTableCornerFront[stateCornerBack][move];
				if (iterativeDeepingDepthSuboptimal(stateCrossFront, stateCrossBack, stateCornerBack, depthFront, depthLeft - 1, move, upperBound))
					return true;
			}
			stateCrossFront = transitionTableCross[stateCrossFront][move];
			stateCornerBack = transitionTableCornerFront[stateCornerBack][move];
		}
		
		return false;
	}
		
	private long computeFullSideIdx(int stateCross, int stateCorner)
	{
		if (stateCorner >= 12096) //7 * 12^3
		{
			stateCross  = inverseTableCross[stateCross];
			stateCorner = inverseTableCorner[stateCorner];
		}
		
		return (stateCorner * 248832L) + stateCross;
	}
	
	public void proveGodNumber12()
	{
		System.out.println("Prove lower bound 12:");
		System.out.println("Scramble: 2 0 4 1 5 1 2 0 4 0 6 7 11 0");
				
		int stateCrossFront = TransitionTableGenerator.stateToIdx(new int[]{0, 1, 5, 1, 0});
		int stateCrossBack  = TransitionTableGenerator.stateToIdx(new int[]{0, 6, 7, 11, 0});
		int stateCornerBack = TransitionTableGenerator.stateToIdx(new int[]{8, 10, 8, 10});	
		
		if (solve(stateCrossFront, stateCrossBack, stateCornerBack) < 12)
		{
			System.out.print("Error");
			return;
		}
		
		System.out.println();
		System.out.println("Prove upper bound 12:");
		
		long entrySize = 3009871872L;
		
		long total1 = 0;
		long total2 = 0;
		long total3 = 0;
		
		for (long idx = 0; idx < entrySize; idx++)
		{
			if (idx % 1000000 == 0)
				System.out.printf("%2.2f percent (total1 = %d, total2 = %d, total3 = %d)\n", (idx / (double)entrySize * 100), total1, total2, total3);
			
			if (backFaceTable.getEntry(idx) != 8)
				continue;
			
			stateCrossFront = 0;
			stateCrossBack  = (int)(idx % 248832);
			stateCornerBack = (int)(idx / 248832);
						
			//undo the last move (UUUU)
			for (int i = 1; i < 12; i++)
			{
				stateCrossFront = transitionTableCross[stateCrossFront][14];
				stateCornerBack = transitionTableCornerFront[stateCornerBack][14];
				
				//solve it in < 1 + 8                             
				total1++;
				if (solveSuboptimal(stateCrossFront, stateCrossBack, stateCornerBack, 8) >  8)
				{
					//undo another move (UUdd)
					int stateCrossFront2 = stateCrossFront;
					int stateCornerBack2 = stateCornerBack;
					for (int j = 1; j < 12; j++)
					{
						stateCrossFront2 = transitionTableCross[stateCrossFront2][4];
						stateCornerBack2 = transitionTableCornerFront[stateCornerBack2][4];
						
						//solve it in < 2 + 8                             
						total2++;
						if (solveSuboptimal(stateCrossFront2, stateCrossBack, stateCornerBack2, 9) >  9)
						{
							//undo another move (Uddd)
							int stateCrossFront3 = stateCrossFront2;
							int stateCornerBack3 = stateCornerBack2;
							for (int k = 1; k < 12; k++)
							{
								stateCrossFront3 = transitionTableCross[stateCrossFront3][0];
								stateCornerBack3 = transitionTableCornerFront[stateCornerBack3][0];
								
								//solve it in < 3 + 8
								total3++;
								if (solveSuboptimal(stateCrossFront3, stateCrossBack, stateCornerBack3, 10) >  10)
								{
									System.out.print("Error");
									return;
								}
							}
						}
					}
				}
			}
		}
		
		System.out.println("God number is 12!");
	}
	
	public static void main(String[] args)
	{
		System.out.println("Optimal Clock Solver");
		System.out.println("by Jakob Kogler (2014)");
		System.out.println();
		
		ClockSolver solver = new ClockSolver();
		Scanner sc = new Scanner(System.in);

		System.out.println();
		System.out.println("Possible inputs: ");
		System.out.println(" * scramble (e.g. 2 0 4 1 5 1 2 0 4 0 6 7 11 0)");
		System.out.println(" * 'random' to solve multiple random scrambles (optimal or suboptimal)");
		System.out.println(" * 'prove12' to prove god's number");
		System.out.println(" * 'quit' to exit");
		
		System.out.println();
		System.out.print(": ");
		while (sc.hasNext())
		{
			
			if (!sc.hasNextInt())
			{
				String command = sc.next();
				if (command.equals("random"))
				{
					System.out.print("number of random scrambles: ");
					int cnt = sc.nextInt();
					
					System.out.print("upper bound for accepted solutions (0 for optimal solves): ");
					int upperBound = sc.nextInt();
					
					solveRandom(solver, cnt, upperBound);
				}
				else if (command.equals("prove12"))
				{
					solver.proveGodNumber12();
				}
				else
					return;
			}
			else
			{
				int[] state = new int[14];
				for (int i = 0; i < 14; i++)
					state[i] = sc.nextInt() % 12;
				
				int stateCrossFront = TransitionTableGenerator.stateToIdx(new int[]{state[1], state[3],  state[4],  state[5],  state[7]});
				int stateCrossBack  = TransitionTableGenerator.stateToIdx(new int[]{state[9], state[10], state[11], state[12], state[13]});
				int stateCornerBack = TransitionTableGenerator.stateToIdx(new int[]{(12 - state[2])%12, (12 - state[0])%12, (12 - state[8])%12, (12 - state[6])%12});	
				
				solver.solve(stateCrossFront, stateCrossBack, stateCornerBack);
			}
			
			System.out.println();
			System.out.print(": ");
		};
	}
	
	public static void solveRandom(ClockSolver solver, int cnt, int upperbound)
	{
		long[] distribution = new long[14];
		
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < cnt; i++)
		{
			int stateCrossFront = (int)(Math.random() * 248832);
			int stateCrossBack = (int)(Math.random() * 248832);
			int stateCornerBack = (int)(Math.random() * 20736);
			
			int value = solver.solveSuboptimal(stateCrossFront, stateCrossBack, stateCornerBack, upperbound);
			
			distribution[value]++;
		}
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		
		TableGenerator.printDistribution(distribution);
		System.out.println("Solving time: " + (duration/1000.0) + " seconds (" + Math.round(cnt/(duration/1000.0)*100)/100.0 + " solves per second)");
	}
}
