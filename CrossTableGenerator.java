public class CrossTableGenerator extends TableGenerator
{
	private int[][] transitionTable;
	
	public CrossTableGenerator(int[][] transitionTable)
	{
		this.transitionTable = transitionTable;
		entrySize = 248832L; // =12^5
		fileName = "cross.table";
		tableName = "CrossTable";
		maxDistance = 5;
		iterativeDeepingDepth = 4;
		clockCnt = 5;
	}
	
	protected int depthFirst(int stateCross, int stateCorner, int depth, int depthLeft, int lastMove)
	{
		int entry = table.getEntry(stateCross);
		
		if (depthLeft == 0)
		{
			if (entry == 5)
			{ //IDA => 5 means not yet visited
				table.setEntry(stateCross, depth);
				return 1;
			}
			else
			{
				return 0;
			}
		}
		
		int cnt = 0;
		for (int move = lastMove + 1; move <= 8; move++)
		{//moves 8 -14 have the same effect on the cross pieces
			for (int moveCnt = 1; moveCnt < 12; moveCnt++)
			{
				stateCross = transitionTable[stateCross][move];
				cnt += depthFirst(stateCross, 0, depth, depthLeft - 1, move);
			}
			stateCross = transitionTable[stateCross][move];
		}
		
		return cnt;
	}
	
	protected int solveOptimal(long idx)
	{
		return -1;
	}
}
