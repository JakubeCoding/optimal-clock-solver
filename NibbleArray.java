import java.util.Arrays;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class NibbleArray
{
	private byte[] array;
	
	public NibbleArray(long size, int initValue)
	{
		int halfSize = (int)(size / 2);
		array = new byte[halfSize];
		if (initValue != 0)
			Arrays.fill(array, (byte)((initValue << 4) + initValue));
	}
	
	public NibbleArray(File file) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		if (!file.exists() || !file.canRead())
			throw new FileNotFoundException();
		
		FileInputStream fileIn = new FileInputStream(file);  
		ObjectInputStream in = new ObjectInputStream(fileIn);  
		//@SuppressWarnings("unchecked")
		array = (byte[])in.readObject();  
		in.close();  
		fileIn.close();	
	}
	
	public int getEntry(long idx)
	{
		if ((idx & 1) == 0)
		{
			return (array[(int)(idx >> 1)] & 0xF0) >> 4;
		}
		else
		{
			return array[(int)(idx >> 1)] & 0x0F;
		}
	}
	
	public void setEntry(long idx, int value)
	{
		int idxHalf = (int)(idx >> 1);
		if ((idx & 1) == 0)
		{
			array[idxHalf] = (byte)((array[idxHalf] & 0x0F) | (value << 4));
		}
		else
		{
			array[idxHalf] = (byte)((array[idxHalf] & 0xF0) | value);
		}
	}
	
	public boolean storeInFile(File file)
	{
		if (file.exists())
			return false;
		
		try {  
			FileOutputStream fileOut = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);  
			out.writeObject(array);  
			out.close(); 
			fileOut.close();
			return true;
		}  
		catch (Exception e) 
		{
			return false;
		}
	}
}
