# Optimal Clock Solver #

A fast optimal (and suboptimal) solver for the Rubik's Clock developed by Jakob Kogler. It proved the god's number for this puzzle (12 moves). 

### Using the program ###

* To start the program, compile it with `javac *.java` and start it with `java -Xmx3072m ClockSolver`. Notice, that the program uses a large amount of RAM (about 3GB). 
* On the first start, the program will generate big lookup tables (about 1.5 GB). This can take around 30 minutes. This tables will be saved in the same folder. 
* If you don't have enough RAM, or don't want to wait for the tables to generate, you may want to use a different program: [OptClock](http://www.speedsolving.com/forum/showthread.php?47747-OptClock-optimal-Rubik-s-Clock-solver "OptClock") by Michael Gottlieb and Ben Whitmore.

### Finding an optimal solution ###

* A scrambled clock can be represented by 14 integers between 0 and 11. Each number represent the hour of one clock piece. The order of the numbers is:

![clock.jpg](https://bitbucket.org/repo/qApgpM/images/822507240-clock.jpg)

### Solving multiple random positions ###

* Write `random`, followed by the number of random positions, followed by an upper bound. 
* The upper bound allowes to solve the scrambles suboptimal. It will stop searching for the optimal solution, if it found a solution less equal than the bound. If there is no solution less equal than the bound, it will solve it optimal. Therefore if you set the upper bound to 0, it will solve all random scrambles optimal. 
* The program can solve about 60 scrambles per second optimal, and abount 200.000 scrambles suboptimal with an upper bound of 12. (Intel(R) Core(TM) i7-4600M CPU @ 2.90GHz)

### Proving god's number 12 ###

* Write `prove12` for redoing the prove of god's number. It will solve a 12-move position optimal, to prove the lower bound, and then eliminates all possible 13-move candidates. 
* This will take about 50 minutes. 

### Contact and more information ###

* Post about God's number at [Speedsolving.com](http://www.speedsolving.com/forum/showthread.php?47822-God-s-Number-for-Clock-found "God's Number for Clock found")