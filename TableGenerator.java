import java.io.File;
public abstract class TableGenerator
{
	protected NibbleArray table;
	protected long entrySize;
	protected String fileName;
	protected String tableName;
	protected int maxDistance;
	protected int iterativeDeepingDepth;
	protected int clockCnt;
	
	public NibbleArray getTable()
	{
		generateTable();
		return table;
	}
	
	/**
	 * Generates for each possible state the distance to the solved position.
	 */
	private void generateTable()
	{
		//Try loading table from file
		System.out.println("Try loading \'" + tableName + "\' from \'" + fileName + "\'.");
		File file = new File(fileName);
		try
		{
			table = new NibbleArray(file);
			System.out.println("Loaded successfully.");
		}
		catch (Exception e)
		{
			System.out.println("Loading failed.");
		
			//Generate table
			System.out.println("Generating table.");
			int tableSize = (int)(entrySize / 2);
			table = new NibbleArray(entrySize, maxDistance);
			long[] distribution = new long[maxDistance + 1];
			distribution[maxDistance] = entrySize;
			int stateCross = 0;
			int stateCorner = 0;
			
			//iterative deeping
			for (int depth = 0; depth <= iterativeDeepingDepth; depth++)
			{
				distribution[depth] = depthFirst(stateCross, stateCorner, depth, depth, -1);
				distribution[maxDistance] -= distribution[depth];
				System.out.print(depth + "   " + distribution[depth] + "\n");
			}
			System.out.println();
			
			//solve unsolved positions
			if (iterativeDeepingDepth + 1 < maxDistance)
			{
				for (long i = 0; i < entrySize; i++)
				{
					if (i % (entrySize/20) == 0)
						System.out.println(Math.round(i/(double)entrySize*100) + " percent");
	
					if (table.getEntry(i) == maxDistance)
					{
						int value = solveOptimal(i);
						table.setEntry(i, value);
						distribution[value]++;
						distribution[maxDistance]--;
					}
				}
			}
			
			System.out.println("\'" + tableName + "\' generated.");
			printDistribution(distribution);
			
			//Try store table in file
			if (table.storeInFile(file))
			{
				System.out.println("\'" + tableName + "\' stored in \'" + fileName + "\'.");
			}
		}
	}
	
	protected abstract int depthFirst(int stateCross, int stateCorner, int depth, int depthLeft, int lastMove);
	
	protected abstract int solveOptimal(long idx);
	
	protected static void printDistribution(long[] distribution)
	{
		long sum = 0;
		long total = 0;
		System.out.println("Distribution:");
		for (int i = 0; i < distribution.length; i++)
		{
			if (distribution[i] != 0)
			{
				System.out.println("Depth " + i + ": " + distribution[i] + " states");
				sum += distribution[i] * i;
				total += distribution[i];
			}
		}
		System.out.printf("Average depth: %2.2f\n\n", sum/(double)total);
	}
}
