public class TransitionTableGenerator
{
	public static int[][] generateTable(int clockCnt, byte[] moveManipulation, boolean inverse)
	{
		int entryCnt = 1;
		for (int i = 0; i < clockCnt; i++)
			entryCnt *= 12;
		
		int[][] table = new int[entryCnt][15];
		
		for (int idx = 0; idx < entryCnt; idx++)
		{
			int[] state = idxToState(idx, clockCnt);
			for (int move = 0; move < 15; move++)
			{
				int[] state2 = applyMove(state, moveManipulation[move], inverse);
				int idx2 = stateToIdx(state2);
				table[idx][move] = idx2;
			}
		}
		
		return table;
	}
	
	public static int[] idxToState(int idx, int clockCnt)
	{
		int[] state = new int[clockCnt];
		for (int i = 0; i < clockCnt; i++)
		{
			state[clockCnt - 1 - i] = idx % 12;
			idx /= 12;
		}
		return state;
	}
	
	public static int stateToIdx(int[] state)
	{
		int idx = 0;
		for (int i = 0; i < state.length; i++)
		{
			idx = 12 * idx + state[i];
		}
		return idx;
	}
	
	private static int[] applyMove(int[] state, byte moveManipulation, boolean inverse)
	{
		int[] state2 = new int[state.length];
		for (int i = 0; i < state.length; i++)
		{
			state2[i] = state[i];
			if ((moveManipulation & (1 << (state.length - 1 - i))) != 0)
				state2[i] = (state[i] + (inverse ? 11 : 1))%12;
		}
		return state2;
	}
}
